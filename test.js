
function hackInitMatriz(maat,tam){
	for (i=0;i<tam;i++){
		maat[i]=[];
		for (j=0;j<tam;j++){
			
			maat[i][j]="*";
		}
	}
}
function hackPrintMatriz(maat){
	ress="";
	for (indx=0;indx<maat.length;indx++){
		for (jndx=0;jndx<maat.length;jndx++){
			ress=ress+maat[indx][jndx]+" ";
		}
		// console.log(ress);
		ress=ress+"\n";
	}
	return ress;
}

function insertarBarco(tbarco,coords,orient,mat){
	tbarcoo=4-tbarco;
	l=Number(coords[0])-1;
	s=Number(coords[2])-1;
	/*console.log(l+" "+s);*/

	if (orient=="v"){
		if (l+tbarcoo<=mat.length){
			for (m=0;m<tbarcoo;m++){
				if (mat[l+m][s]!="*")
					return false
			}
			for (m=0;m<tbarcoo;m++){
				mat[l+m][s]=(5-tbarcoo).toString();

			}
		}
		else
			return false;//no cabe
			
	}
	else{
		if (s+tbarcoo<=mat.length){
			for (m=0;m<tbarcoo;m++){
				if (mat[l][s+m]!="*")
					return false
			}
			for (m=0;m<tbarcoo;m++){
				mat[l][s+m]=(5-tbarcoo).toString();

			}
		}
		else
			return false;//no cabe
			
	}
	return true;
}

function crearJugador(ladoo, njugador){
	var matriz=[];
	hackInitMatriz(matriz,ladoo);
	alert("Jugador "+njugador+", coloca tus barcos");
	var barcos="";
	var coordenadas="";
	var k=0;
	for (i=0;i<3;i++){
		coordenadas=prompt("Introduzca las coordenadas (X:Y) del barco T"+(i+1));
		orient=prompt("Introduzca la orientacion del barco (V ó H)");
		orient=orient.toLowerCase();

		for (k=0;coordenadas[k]!=":" && k<3;k++)
			{/*Comprobar que coordendas buenas*/}
		if (k==1){
			if (!insertarBarco(i/*Tipo de Barco*/,coordenadas,orient,matriz)){
				alert("no cabe el barco! insertar de nuevo:");
				i=i-1;
			}
			else
				alert(hackPrintMatriz(matriz));
			//else 
			//INSERTO BARCO FELICIDADES!
		}
		else{

			alert("no cabe el barco por coordenadas malas! insertar de nuevo:");
			i=i-1;
		}
	}
	return matriz;
}


//BATALLA--------------------------------------------------
function terminado(m1,m2){
	cont1=0;
	cont2=0;
	for (i=0;i<m1.length;i++){
		for (j=0;j<m1.length;j++){
			if (m1[i][j]!="*" && m1[i][j]!="f" && m1[i][j]!="x")
				cont1++;
			if (m2[i][j]!="*" && m2[i][j]!="f" && m2[i][j]!="x")
				cont2++;

			if (cont1>0 && cont2>0)
				return false;//VERIFICA SI EN UNO DE LOS TABLEROS YA NO HAY BARCOS
		}
	}
	return true;//VERIFICA SI EN UNO DE LOS TABLEROS YA NO HAY BARCOS
}

function getWinner(m1,m2,p1,p2){
	cont1=0;
	cont2=0;
	for (i=0;i<m1.length;i++){
		for (j=0;j<m1.length;j++){
			if (m1[i][j]!="*" && m1[i][j]!="f" && m1[i][j]!="x")
				cont1++;
			if (m2[i][j]!="*" && m2[i][j]!="f" && m2[i][j]!="x")
				cont2++;

			if (cont1>0 && cont2>0)
				return false;//NO SE EJECUTA ESTE RETURN...
		}
	}
	if (cont1==0)
		return p2;//RETORNA EL JUGADOR QUE TENGA BARCOS EN SU TABLERO (GANADOR)
	else
		return p1;//RETORNA EL JUGADOR QUE TENGA BARCOS EN SU TABLERO (GANADOR)
	return true;//NO SE EJECUTA ESTE RETURN...
}

function comprobarHundido(mmmat,valor){
	var auxcont=0;
	for (auxi=0;auxi<mmmat.length;auxi++){
		for (auxj=0;auxj<mmmat.length;auxj++){
			if (mmmat[auxi][auxj]==valor)
				return false;
		}
	}
	return true;//BUSCA EN TODA LA MATRIZ SI EL VARCO "valor" FUE HUNDIDO
}

function hackPrintMatrizBatalla(maat){
	ress="";
	for (aui=0;aui<maat.length;aui++){
		for (auj=0;auj<maat.length;auj++){
			if (maat[aui][auj]!="*" && maat[aui][auj]!="f" && maat[aui][auj]!="x")
				ress=ress+"* ";
			else
				ress=ress+maat[aui][auj]+" ";
		}
		
		ress=ress+"\n";
	}
	return ress;
}

function atacar(mmat,pj){
	
	while (true){//SE ROMPE EN LO QUE SEA UN ATAQUE EXITOSO

		alert("Turno del Jugador "+pj+"\n"+hackPrintMatrizBatalla(mmat));
		cords=prompt("Introduzca las coordenadas X:Y para el ataque");//TERMINAR
		s=Number(cords[0])-1;
		l=Number(cords[2])-1;

		var k=0;
		for (k=0;cords[k]!=":" && k<3;k++)
		{/*k de comprobacion de COORDENADAS BUENAS*/ }
		if (k==1 && s<mmat.length && l<mmat.length){
			if (mmat[s][l]=="*")
				mmat[s][l]="f";
			else if (mmat[s][l]=="f")
				mmat[s][l]="f";
			else if (mmat[s][l]=="x")
				mmat[s][l]="x";
			else{
				auxval=mmat[s][l];
				mmat[s][l]="x";
				if (comprobarHundido(mmat,auxval)){
					alert("Barco Hundido: T"+auxval);
				}
			}
			//ATAQUE EXITOSO
			alert("Status (board enemigo):\n"+hackPrintMatrizBatalla(mmat));
			break;
		}
		else{//ATAQUE NO EXITOSO POR COORDENADAS MALAS
			alert("Coordenadas malas! insertar nuevamente"+
				"Status (board enemigo):\n"+hackPrintMatrizBatalla(mmat));
			
		}
		
	}
	

}

function batalla(m1,m2,p1,p2){
	turno=p2;
	var ganador;
	while(!terminado(m1,m2)){
		if (turno==p2){
			turno=p1;
			atacar(m2,p1);
		}
		else{
			turno=p2;
			atacar(m1,p2);
		}
	}

	ganador=getWinner(m1,m2,p1,p2);//super ineficiente =( el hecho de hacer la funcion getWinner()
		// cuando se parece mucho a terminado()

	alert("Terminado, Ganador: "+ ganador
		+"\nStatus de tableros: \nTablero Jugador "+p1+":\n"+hackPrintMatriz(m1)+
		"\nTablero Jugador "+p2+":\n"+hackPrintMatriz(m2));
}
//BATALLA---------------------------- END

function main (){
	var lado=Number(prompt("Tamaño del tablero:"));
	var matriz1=[];
	var matriz2=[];
	var jugador1=prompt("Inserta Nombre del Jugador:");
	var jugador2=prompt("Inserta Nombre del siguiente Jugador:");
	matriz1=crearJugador(lado,jugador1);
	alert("Matriz Jugador 1\n"+hackPrintMatriz(matriz1));

	matriz2=crearJugador(lado,jugador2);
	alert("Matriz Jugador 2\n"+hackPrintMatriz(matriz2));

	batalla(matriz1,matriz2,jugador1,jugador2);
	/*asdf*/

}



 main();

